package com.rostyslavprotsiv.model.action;

import java.util.Arrays;

public class StringUtils<T> {
    public String concat(T... params) {
        StringBuilder result = new StringBuilder();
        Arrays.stream(params).forEach(s -> result.append(s.toString()));
        return result.toString();
    }


}
