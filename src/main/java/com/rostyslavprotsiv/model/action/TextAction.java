package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Sentence;
import com.rostyslavprotsiv.model.entity.Text;
import com.rostyslavprotsiv.model.entity.TextEntity;
import com.rostyslavprotsiv.model.entity.Word;

import java.util.Arrays;
import java.util.stream.Stream;

public class TextAction {
    public void deleteAllWordLettersAsFirst(Text text) {
        Stream.of(text.getSentences())
                .flatMap(s -> Stream.of(s.getSentence()))
                .filter(b -> b instanceof Word)
                .forEach(this::deleteNext);
    }

    public Text reallocateSentencesByWordsNumber(Text text) {
         Sentence[] sentences = Stream.of(text.getSentences())
                .sorted(this::compare).toArray(Sentence[]::new);
         return new Text(sentences);
    }

    private int compare(Sentence sen, Sentence sen1) {
        TextEntity[] wordArray = Arrays.stream(sen.getSentence())
                .filter(b -> b instanceof Word).toArray(TextEntity[]::new);
        TextEntity[] wordArray1 = Arrays.stream(sen1.getSentence())
                .filter(b -> b instanceof Word).toArray(TextEntity[]::new);
        return wordArray.length - wordArray1.length;
    }

    private void deleteNext(TextEntity word) {
        String firstLetter = String.valueOf(word.getText().charAt(0));
        StringBuilder result = new StringBuilder(firstLetter);
        result.append(word.getText().replaceAll(firstLetter, ""));
        word.setText(result.toString());
    }
}
