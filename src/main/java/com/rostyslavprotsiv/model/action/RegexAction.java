package com.rostyslavprotsiv.model.action;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexAction {
    public boolean checkSentenceCapitalBeginPeriodEnd(String sentence) {
        Pattern p = Pattern.compile("^[A-Z].*\\.$");
        Matcher m = p.matcher(sentence);
        return m.matches();
    }

    public String[] splitTextOnThe(String text) {
        Pattern p = Pattern.compile("the");
        return p.split(text);
    }

    public String replaceVowelsWithUnderscores(String text) {
        StringBuilder result = new StringBuilder();
        Pattern p = Pattern.compile("[eyiuoaEYUIOA]");
        Matcher m = p.matcher(text);
        int previous = 0;
        while (m.find()) {
            result.append(text, previous, m.start());
            result.append("_");
            previous = m.end();
        }
        result.append(text, previous, text.length());
        return result.toString();
    }
}
