package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class TextParser {

    public Text parse(String text) {
        List<Sentence> sentences = new LinkedList<>();
        List<TextEntity> sentence = new LinkedList<>();
        Stream.of(text.split("\\s+"))
                .flatMap(this::getSeparated)
                .map(this::convertIntoTextEntityStream)
                .forEach(a -> fillIn(a, sentences, sentence));
        return new Text(sentences.toArray(new Sentence[0]));
    }

    private Stream<String> getSeparated(String s) {
        String[] newArr = new String[2];// elements of word and mark
        Pattern p = Pattern.compile("[,.?!:;]+");
        Matcher m = p.matcher(s);
        if (m.find()) {
            newArr[0] = s.substring(0, m.start());
            newArr[1] = s.substring(m.start(), m.end());
        } else {
            return Arrays.stream(new String[]{s});
        }
        return Arrays.stream(newArr);
    }

    private TextEntity convertIntoTextEntityStream(String s) {
        Pattern p = Pattern.compile("[,.?!:;]+");
        Matcher m = p.matcher(s);
        if (m.matches()) {
            return new PunctuationMark(s);
        } else {
            return new Word(s);
        }
    }

    private void fillIn(TextEntity entity, List<Sentence> sentences,
                            List<TextEntity> sentence) {
        sentence.add(entity);
        Pattern p = Pattern.compile("[.?!]+");
        Matcher m = p.matcher(entity.getText());
        if (m.matches()) {
            sentences.add(new Sentence(sentence.toArray(new TextEntity[0])));
            sentence.clear();
        }
    }
}
