package com.rostyslavprotsiv.model.action;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MyFileReader {
    public String read(String fileName) throws IOException {
        StringBuilder result = new StringBuilder();
        String tmp;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((tmp = br.readLine()) != null) {
                result.append(tmp);
                result.append("\n");
            }
        }
        return result.toString();
    }
}
