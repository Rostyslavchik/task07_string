package com.rostyslavprotsiv.model.entity;

public abstract class TextEntity {
    protected String text;

    public TextEntity() {}

    public TextEntity(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public abstract String toString();
}
