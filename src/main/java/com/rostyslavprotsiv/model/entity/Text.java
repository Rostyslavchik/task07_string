package com.rostyslavprotsiv.model.entity;

import java.util.Arrays;

public class Text {
    private Sentence[] sentences;

    public Text() {}

    public Text(Sentence[] sentences) {
        this.sentences = sentences;
    }

    public Sentence[] getSentences() {
        return sentences;
    }

    public void setSentences(Sentence[] sentences) {
        this.sentences = sentences;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Text other = (Text) obj;
        if (sentences == null) {
            if (other.sentences != null) {
                return false;
            }
        } else if (!Arrays.equals(sentences, other.sentences)) {
            return  false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) (((sentences == null) ? 0 : Arrays.hashCode(sentences)));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Arrays.stream(sentences).forEach(result::append);
        return result.toString();
    }


//    @Override
//    public String toString() {
//        return "Text{" +
//                "sentences=" + Arrays.toString(sentences) +
//                '}';
//    }
}
