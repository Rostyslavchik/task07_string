package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class Word extends TextEntity {

    public Word() {}

    public Word(String word) {
        super(word);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return Objects.equals(text, word1.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }

    @Override
    public String toString() {
        return " " + text;
    }

//    @Override
//    public String toString() {
//        return "Word{" +
//                "word='" + text+ '\'' +
//                '}';
//    }
}
