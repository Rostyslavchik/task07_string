package com.rostyslavprotsiv.model.entity;

import java.util.Arrays;

public class Sentence {
    private TextEntity[] sentence;

    public Sentence() {}

    public Sentence(TextEntity[] sentence) {
        this.sentence = sentence;
    }

    public TextEntity[] getSentence() {
        return sentence;
    }

    public void setSentence(TextEntity[] sentence) {
        this.sentence = sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return Arrays.equals(sentence, sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(sentence);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Arrays.stream(sentence).forEach(result::append);
        result.append("\n");
        return result.toString();
    }

//    @Override
//    public String toString() {
//        return "Sentence{" +
//                "sentence=" + Arrays.toString(sentence) +
//                '}';
//    }
}
