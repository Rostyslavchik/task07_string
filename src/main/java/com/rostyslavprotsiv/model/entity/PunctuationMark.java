package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class PunctuationMark extends TextEntity {

    public PunctuationMark() {}

    public PunctuationMark(String mark) {
        super(mark);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PunctuationMark that = (PunctuationMark) o;
        return Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }

    @Override
    public String toString() {
        return text;
    }


//    @Override
//    public String toString() {
//        return "PunctuationMark{" +
//                "mark='" + text + '\'' +
//                '}';
//    }
}
