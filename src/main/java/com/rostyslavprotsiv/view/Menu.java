package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.view.manager.ResourceManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);
    private static final ResourceManager RESOURCE_MANAGER
            = ResourceManager.INSTANCE;

    public void welcome() {
        LOGGER.info(RESOURCE_MANAGER.getString("welcome"));
    }

    public void outConcatExample(String before, String after) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.first.beforeconcat") + before);
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.first.afterconcat") + after);
    }

    public void outRegexSentenceExample(String sentence,
                                        boolean isAppropriate) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.second.condition.part1") + sentence + "|(end)");
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.second.condition.part2") + isAppropriate);
    }

    public void outSplitExample(String sentenceBefore, String[] arrayAfter) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.third.condition.part1") + sentenceBefore + "|(end)");
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.third.condition.part2")
                + Arrays.toString(arrayAfter));
    }

    public void outReplaceExample(String sentenceBefore, String sentenceAfter) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.fourth.condition.part1") + sentenceBefore + "|(end)");
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task.fourth.condition.part2") + sentenceAfter + "|(end)");
    }

    public void outText(String text) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task2.alltext") + text);
    }

    public void outTextWithDeletedLetters(String text) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task2.alltext.deleted") + text);
    }

    public void outSortedTextByWordNumbers(String text) {
        LOGGER.info(RESOURCE_MANAGER.getString(
                "task2.alltext.sorted") + text);
    }
}
