package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import com.rostyslavprotsiv.model.entity.Text;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final Menu MENU = new Menu();
    private static final StringUtils STRING_UTILS = new StringUtils();
    private static final RegexAction REGEX_ACTION = new RegexAction();
    private static final TextAction TEXT_ACTION = new TextAction();
    public static String sentence = "hThis the is the Spaaar the taa. tt";
//    private static final StringUtils<List> STRING_UTILS = new StringUtils();

    public void execute() {
        List<Integer> listBefore = Arrays.asList(1, 2, 3);
        List<Integer> listBefore1 = Arrays.asList(4, 5);
        List<Integer> listBefore2 = Arrays.asList(6, 7, 8, 9, 51);
        StringBuilder before = new StringBuilder();
        boolean isTrue = REGEX_ACTION
                .checkSentenceCapitalBeginPeriodEnd(sentence);
        String[] split;
        String sentenceAfterReplacement;
        MENU.welcome();
        before.append(listBefore);
        before.append(" ");
        before.append(listBefore1);
        before.append(" ");
        before.append(listBefore2);
        before.append(" ");
        String after = STRING_UTILS.concat(listBefore,
                listBefore1, listBefore2, before);
        MENU.outConcatExample(before.toString(), after);
        MENU.outRegexSentenceExample(sentence, isTrue);
        split = REGEX_ACTION.splitTextOnThe(sentence);
        MENU.outSplitExample(sentence, split);
        sentenceAfterReplacement = REGEX_ACTION
                .replaceVowelsWithUnderscores(sentence);
        MENU.outReplaceExample(sentence, sentenceAfterReplacement);
        demonstrateTextExample();
    }

    private Text readAndParse(String filePath) {
        MyFileReader reader = new MyFileReader();
        String text = null;
        try {
            text = reader.read(filePath);
        } catch (IOException e) {
            LOGGER.error(e.getMessage() + " " + e.getCause());
        }
        TextParser parser = new TextParser();
        return parser.parse(text);
    }

    private void demonstrateTextExample() {
        Text text = readAndParse("src/main/resources/sometext.txt");
        MENU.outText(text.toString());
        TEXT_ACTION.deleteAllWordLettersAsFirst(text);
        MENU.outTextWithDeletedLetters(text.toString());
        text = TEXT_ACTION.reallocateSentencesByWordsNumber(text);
        MENU.outSortedTextByWordNumbers(text.toString());
    }
}
